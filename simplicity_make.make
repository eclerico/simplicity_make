; download dependencies for the courses.simplicitycalifornia.org site
core = 7.x

api = 2
projects[] = "drupal"

projects[link][version] = 1.2
projects[link][type] = "module"
projects[link][subdir] = "contrib/"

projects[views_slideshow][version] = 3.1
projects[views_slideshow][type] = "module"
projects[views_slideshow][subdir] = "contrib/"

projects[simplicity_zen][download][type] = "git"
projects[simplicity_zen][download][url] = "myrepo:eclerico/simplicity_zen.git"
projects[simplicity_zen][type] = "theme"

projects[simplicity_module][download][type] = "git"
projects[simplicity_module][download][url] = "myrepo:eclerico/simplicitycalifornia_module.git"
projects[simplicity_module][type] = "module"
projects[simplicity_module][subdir] = "custom/"

libraries[jquery_cycle][download][type] = get
libraries[jquery_cycle][download][url] = http://malsup.github.com/jquery.cycle.all.js
libraries[jquery_cycle][directory_name] = jquery.cycle